package com.sample.viewer.ui.main;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.sample.viewer.R;
import com.sample.viewer.databinding.ActivityMainBinding;
import com.sample.viewer.module.ApplicationComponentHolder;
import com.sample.viewer.ui.base.BaseActivity;
import com.sample.viewer.ui.list.ListFragment;
import com.sample.viewer.utils.FragmentUtils;


public class MainActivity extends BaseActivity<ActivityMainBinding> {

    @Override
    public int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            int stackHeight = getSupportFragmentManager().getBackStackEntryCount();
            if (stackHeight > 0) { // if we have something on the stack (doesn't include the current shown fragment)
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            } else {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setHomeButtonEnabled(false);
            }
        });
    }

    @Override
    protected void inject() {
        ApplicationComponentHolder.getInstance().getComponent().inject(this);
        FragmentUtils.replaceFragment(this, new ListFragment(), R.id.container, false);
    }

}
