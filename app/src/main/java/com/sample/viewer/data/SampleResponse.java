package com.sample.viewer.data;

import java.util.List;

public class SampleResponse {
    private List<Topics> RelatedTopics;

    public List<Topics> getRelatedTopics() {
        return RelatedTopics;
    }

    public void setRelatedTopics(List<Topics> relatedTopics) {
        RelatedTopics = relatedTopics;
    }
}
