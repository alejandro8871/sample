package com.sample.viewer.ui.list;

import android.annotation.SuppressLint;

import androidx.lifecycle.ViewModel;

import com.sample.viewer.data.SampleRepository;
import com.sample.viewer.data.SampleResponse;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class ListViewModel extends ViewModel {

    private SampleRepository sampleRepository;
    private PublishSubject<SampleResponse> sampleResponsePublishSubject = PublishSubject.create();

    @Inject
    public ListViewModel(SampleRepository sampleRepository) {
        this.sampleRepository = sampleRepository;
    }

    public Observable<SampleResponse> getProductsResult() {
        return sampleResponsePublishSubject;
    }

    @SuppressLint("CheckResult")
    public void getProducts() {
        sampleRepository.getProducts()
                .subscribeOn(Schedulers.io())
                .subscribe(sampleResponse -> sampleResponsePublishSubject.onNext(sampleResponse));
    }
}
