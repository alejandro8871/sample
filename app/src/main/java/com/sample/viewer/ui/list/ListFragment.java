package com.sample.viewer.ui.list;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.sample.viewer.R;
import com.sample.viewer.data.SampleResponse;
import com.sample.viewer.data.Topics;
import com.sample.viewer.databinding.FragmentListBinding;
import com.sample.viewer.module.ApplicationComponentHolder;
import com.sample.viewer.ui.base.BaseFragment;
import com.sample.viewer.ui.detail.DetailFragment;
import com.sample.viewer.ui.list.adapter.AdapterList;
import com.sample.viewer.utils.FragmentUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class ListFragment extends BaseFragment<ListViewModel, FragmentListBinding>
        implements AdapterList.TopicsListCallback {

    private AdapterList adapterList;

    @Override
    protected Class<ListViewModel> getViewModel() {
        return ListViewModel.class;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_list;
    }

    @Override
    protected void inject() {
        ApplicationComponentHolder.getInstance().getComponent().inject(this);
    }

    @Override
    protected void subscribeOnStart() {
        super.subscribeOnStart();
        addSubscription(viewModel().getProductsResult()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleProductResult));
        viewModel.getProducts();
    }

    private void handleProductResult(SampleResponse sampleResponse) {
        adapterList.setData(sampleResponse.getRelatedTopics());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        adapterList = new AdapterList(this);
        dataBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        dataBinding.recyclerView.setAdapter(adapterList);
        return dataBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onArticleClicked(Topics articleEntity) {
        if (null != getActivity()) {
            FragmentUtils.replaceFragment((AppCompatActivity) getActivity(),
                    DetailFragment.newInstance(articleEntity), R.id.container, true);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (null == getActivity())
            return;

        SearchView searchView;
        getActivity().getMenuInflater().inflate(R.menu.menu_main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();

        assert searchManager != null;
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getActivity().getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(null != dataBinding.recyclerView.getAdapter())
                    ((AdapterList)dataBinding.recyclerView.getAdapter()).getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(null != dataBinding.recyclerView.getAdapter())
                    ((AdapterList)dataBinding.recyclerView.getAdapter()).getFilter().filter(newText);
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
