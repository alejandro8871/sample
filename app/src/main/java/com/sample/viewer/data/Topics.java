package com.sample.viewer.data;

import android.os.Parcel;
import android.os.Parcelable;

public class Topics implements Parcelable {

    private String Text;
    private String Result;
    private String FirstURL;
    private Icon Icon;


    protected Topics(Parcel in) {
        Text = in.readString();
        Result = in.readString();
        FirstURL = in.readString();
        Icon = in.readParcelable(com.sample.viewer.data.Icon.class.getClassLoader());
    }

    public static final Creator<Topics> CREATOR = new Creator<Topics>() {
        @Override
        public Topics createFromParcel(Parcel in) {
            return new Topics(in);
        }

        @Override
        public Topics[] newArray(int size) {
            return new Topics[size];
        }
    };

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String getFirstURL() {
        return FirstURL;
    }

    public void setFirstURL(String firstURL) {
        FirstURL = firstURL;
    }

    public Icon getIcon() {
        return Icon;
    }

    public void setIcon(Icon icon) {
        Icon = icon;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Text);
        parcel.writeString(Result);
        parcel.writeString(FirstURL);
        parcel.writeParcelable(Icon, i);
    }
}
