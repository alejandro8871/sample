package com.sample.viewer.module;


import com.sample.viewer.BaseApplication;
import com.sample.viewer.ui.detail.DetailFragment;
import com.sample.viewer.ui.list.ListFragment;
import com.sample.viewer.ui.main.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        AppModule.class,
        NetworkModule.class,
        ViewModelModule.class
})
public interface ApplicationComponent {

    void inject(BaseApplication baseApplication);

    void inject(MainActivity mainActivity);

    void inject(ListFragment listFragment);

    void inject(DetailFragment detailFragment);
}
