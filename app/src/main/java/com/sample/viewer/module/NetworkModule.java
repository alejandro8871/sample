package com.sample.viewer.module;

import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.sample.viewer.services.SampleService;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.sample.viewer.BuildConfig.BASE_URL;

@Module
public class NetworkModule {
    @Provides
    @Singleton
    SampleService providesNetworkService(Gson gson) {
        OkHttpClient.Builder okhttpclientBuilder = new OkHttpClient.Builder()
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .connectTimeout(1, TimeUnit.MINUTES);

        OkHttpClient okHttpClient = okhttpclientBuilder.build();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();

        return retrofit.create(SampleService.class);
    }

    @Provides
    @Singleton
    Gson providesGson() {
        return new GsonBuilder().create();
    }

    @Provides
    @Singleton
    WebViewClient providesWebViewClient() {
        return new WebViewClient() {
            @Override
            public void onReceivedError(
                    WebView webView,
                    WebResourceRequest request,
                    WebResourceError error) {
            }
        };
    }
}
