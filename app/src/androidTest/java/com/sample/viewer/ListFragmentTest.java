package com.sample.viewer;

import androidx.test.espresso.Espresso;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.sample.viewer.ui.list.ListFragment;
import com.sample.viewer.ui.main.MainActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;


@SuppressWarnings("unchecked")
@RunWith(AndroidJUnit4.class)
public class ListFragmentTest {

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule(MainActivity.class);

    @Before
    public void init() {
        activityRule.getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new ListFragment()).commit();
    }

    @Test
    public void OnRecyclerViewClick() {
        Espresso.onView(withId(R.id.recyclerView)).perform(click());
    }

    @Test
    public void onSearchClick() {
        Espresso.onView(withId(R.id.action_search)).perform(click());
    }
}
