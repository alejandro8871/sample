package com.sample.viewer.module;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.sample.viewer.ui.base.ViewModelFactory;
import com.sample.viewer.ui.detail.DetailViewModel;
import com.sample.viewer.ui.list.ListViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(ListViewModel.class)
    @SuppressWarnings("unused")
    abstract ViewModel bindsListViewModel(ListViewModel listViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel.class)
    @SuppressWarnings("unused")
    abstract ViewModel bindsDetailViewModel(DetailViewModel detailViewModel);

    @Binds
    @SuppressWarnings("unused")
    abstract ViewModelProvider.Factory bindsViewModelFactory(ViewModelFactory viewModelFactory);
}
