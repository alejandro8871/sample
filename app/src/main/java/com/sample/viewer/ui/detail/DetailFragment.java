package com.sample.viewer.ui.detail;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sample.viewer.R;
import com.sample.viewer.data.Topics;
import com.sample.viewer.databinding.FragmentDetailBinding;
import com.sample.viewer.module.ApplicationComponentHolder;
import com.sample.viewer.ui.base.BaseFragment;

import java.util.Objects;

public class DetailFragment extends BaseFragment<DetailViewModel, FragmentDetailBinding> {
    private final static String TOPICS = "TOPICS";

    public static DetailFragment newInstance(Topics topics) {
        DetailFragment myFragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(TOPICS, topics);
        myFragment.setArguments(args);
        return myFragment;
    }

    @Override
    protected Class<DetailViewModel> getViewModel() {
        return DetailViewModel.class;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_detail;
    }

    @Override
    protected void inject() {
        ApplicationComponentHolder.getInstance().getComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        assert getArguments() != null;
        Topics topics = getArguments().getParcelable(TOPICS);
        dataBinding.textTitle.setText(topics.getText().split("-")[0].trim());
        dataBinding.textContent.setText(Html.fromHtml(topics.getResult()));
        Glide.with(dataBinding.getRoot().getContext())
                .load(topics.getIcon().getURL()).apply(new RequestOptions().centerInside())
                .into(dataBinding.ivProfileImage);
        return dataBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Objects.requireNonNull(getActivity()).onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
