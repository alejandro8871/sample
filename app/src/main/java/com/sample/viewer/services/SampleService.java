package com.sample.viewer.services;

import com.sample.viewer.BuildConfig;
import com.sample.viewer.data.SampleResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface SampleService {

    @GET(BuildConfig.URL_TOPICS)
    Single<SampleResponse> getTopics();
}
