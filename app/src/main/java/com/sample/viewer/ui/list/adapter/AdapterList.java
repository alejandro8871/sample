package com.sample.viewer.ui.list.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sample.viewer.data.Topics;
import com.sample.viewer.databinding.ItemProductListBinding;
import com.sample.viewer.ui.base.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

public class AdapterList extends BaseAdapter<AdapterList.ProductViewHolder, Topics> {

    private List<Topics> products;
    private List<Topics> productsFilter;

    private final TopicsListCallback topicsCallback;


    public AdapterList(@NonNull TopicsListCallback topicsCallback) {
        products = new ArrayList<>();
        productsFilter = new ArrayList<>();
        this.topicsCallback = topicsCallback;
    }

    @Override
    public void setData(List<Topics> products) {
        this.products = products;
        this.productsFilter = products;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return ProductViewHolder.create(LayoutInflater.from(viewGroup.getContext()), viewGroup, topicsCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder viewHolder, int i) {
        viewHolder.onBind(productsFilter.get(i));
    }

    @Override
    public int getItemCount() {
        return productsFilter.size();
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    productsFilter = products;
                } else {
                    List<Topics> filteredList = new ArrayList<>();
                    for (Topics row : products) {

                        // name match condition. this might differ depending on your requirement
                        if (row.getText().split("-")[0].toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    productsFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = productsFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                productsFilter = (ArrayList<Topics>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    static class ProductViewHolder extends RecyclerView.ViewHolder {

        private static ProductViewHolder create(LayoutInflater inflater, ViewGroup parent, TopicsListCallback callback) {
            ItemProductListBinding itemListBinding = ItemProductListBinding.inflate(inflater, parent, false);
            return new ProductViewHolder(itemListBinding, callback);
        }

        final ItemProductListBinding binding;

        private ProductViewHolder(ItemProductListBinding binding, TopicsListCallback callback) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(v ->
                    callback.onArticleClicked(binding.getProducts()));
        }

        private void onBind(Topics articleEntity) {
            binding.setProducts(articleEntity);
            binding.titleTxt.setText(articleEntity.getText().split("-")[0].trim());
            binding.executePendingBindings();
        }
    }

    public interface TopicsListCallback {
        void onArticleClicked(Topics articleEntity);
    }
}
