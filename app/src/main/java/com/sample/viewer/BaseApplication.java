package com.sample.viewer;

import android.app.Application;

import com.sample.viewer.module.AppModule;
import com.sample.viewer.module.ApplicationComponentHolder;
import com.sample.viewer.module.DaggerApplicationComponent;


public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ApplicationComponentHolder.getInstance().setComponent(DaggerApplicationComponent.builder()
                .appModule(new AppModule(this))
                .build());
        ApplicationComponentHolder.getInstance().getComponent().inject(this);
    }
}
