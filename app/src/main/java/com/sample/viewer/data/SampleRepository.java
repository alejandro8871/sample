package com.sample.viewer.data;

import android.annotation.SuppressLint;

import com.sample.viewer.services.SampleService;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SampleRepository {
    private final SampleService apiService;

    @Inject
    SampleRepository(SampleService service) {
        this.apiService = service;
    }

    public Single<SampleResponse> getProducts() {
        return apiService.getTopics();
    }

    @SuppressLint("CheckResult")
    public void loadArticleDetails() {
        Observable.fromCallable(() -> {
            return false;
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                        },
                        (error -> {
                        }));

    }
}
