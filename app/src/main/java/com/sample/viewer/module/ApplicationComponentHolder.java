package com.sample.viewer.module;

public class ApplicationComponentHolder {

    public static final String TAG = ApplicationComponentHolder.class.getSimpleName();

    private static final ApplicationComponentHolder INSTANCE = new ApplicationComponentHolder();

    private ApplicationComponent component;

    private ApplicationComponentHolder() {
    }

    public static ApplicationComponentHolder getInstance() {
        return INSTANCE;
    }

    public ApplicationComponent getComponent() {
        return component;
    }

    public void setComponent(ApplicationComponent applicationComponent) {
        this.component = applicationComponent;
    }
}
