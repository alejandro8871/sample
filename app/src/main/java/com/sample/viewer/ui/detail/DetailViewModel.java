package com.sample.viewer.ui.detail;

import androidx.lifecycle.ViewModel;

import com.sample.viewer.data.SampleRepository;
import com.sample.viewer.data.SampleResponse;

import javax.inject.Inject;

/**
 * ArticleDetails view model
 * <p>
 * Author: Lajesh D
 * Email: lajeshds2007@gmail.com
 * Created: 7/24/2018
 * Modified: 7/24/2018
 */

public class DetailViewModel extends ViewModel {

    private SampleRepository sampleRepository;

    @Inject
    public DetailViewModel(SampleRepository sampleRepository) {
        this.sampleRepository = sampleRepository;
    }

    public void loadArticleDetails() {

        if (null != sampleRepository) {
            sampleRepository.loadArticleDetails();
        }
    }
}
